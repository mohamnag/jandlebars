package com.mohamnag.jandlebars;

import com.oracle.truffle.js.lang.JavaScriptLanguage;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.HostAccess;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyExecutable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class Jandlebars {

  private final Context pgContext;
  private final ByteArrayOutputStream outputStream;
  private final Value hbsCompiler;

  public Jandlebars() {
    this(false);
  }

  public Jandlebars(boolean recordConsoleOutput) {
    // init engine and context
    var builder = Context
      .newBuilder(JavaScriptLanguage.ID)
      .allowHostAccess(HostAccess.ALL)
      .allowHostClassLookup(s -> true);

    if (recordConsoleOutput) {
      outputStream = new ByteArrayOutputStream();
      builder.out(outputStream);

    } else {
      outputStream = null;
    }

    this.pgContext = builder.build();

    try {
      // load handlebars.js
      pgContext
        .eval(Source
          .newBuilder("js", Paths.get("src/main/resources/handlebars.js").toFile())
          .build()
        );

      // init HBS compiler (for inline template compiling)
      this.hbsCompiler = pgContext.eval("js", "template => Handlebars.compile(template)");

    } catch (IOException e) {
      throw new RuntimeException("could not load Handlebar's JavaScript file", e);
    }
  }

  /**
   * Compile template from source.
   *
   * @param template source of template
   * @return compiled template
   */
  public Template compileTemplate(String template) {
    Value templateFunction = hbsCompiler.execute(template);

    if (!templateFunction.canExecute()) {
      throw new RuntimeException("expected an executable result!");
    }

    return new Template(new ProxyGen(pgContext), templateFunction);
  }

  /**
   * Compile template from a file.
   *
   * @param path location of template file
   * @return compiled template
   * @throws IOException if file could not be loaded
   */
  public Template compileTemplate(Path path) throws IOException {
    return compileTemplate(Files.readString(path));
  }

  /**
   * Register JavaScript helper from its source.
   *
   * @param helperName under which it will be accessible in templates
   * @param helper     helper's source <code>function (...) {}</code>
   */
  public void registerJavaScriptHelper(String helperName, String helper) {
    pgContext.eval(
      JavaScriptLanguage.ID,
      "Handlebars.registerHelper('%s', %s)".formatted(helperName, helper)
    );
  }

  /**
   * Register JavaScript helper from a file. Should use Hanldebars' own API
   * for registration.
   *
   * <code>
   * Handlebars.registerHelper('name', function (...) {})
   * </code>
   *
   * @param path location of the JavaScript file
   * @throws IOException if file could not be loaded
   */
  public void registerJavaScriptHelper(Path path) throws IOException {
    pgContext
      .eval(Source
        .newBuilder(JavaScriptLanguage.ID, path.toFile())
        .build()
      );
  }

  /**
   * Register a Java helper.
   *
   * @param helperName under which it will be accessible in templates
   * @param helper     helper implementation
   */
  public void registerJavaHelper(String helperName, Helper helper) {
    pgContext
      .eval(JavaScriptLanguage.ID,
        """
          (function (name, helper) {
            var helperProxy = new Proxy(function () {}, { apply: helper })
            Handlebars.registerHelper(name, helperProxy)
          })
          """)
      .execute(
        helperName,
        (ProxyExecutable) (Value... args) ->
          // this gets 3 args from Proxy, first is the empty function context,
          // second is the helper's context, third has both the helper's context
          // but also the helper options object.
          helper.apply(
            args[2].getArrayElement(0),
            args[2].getArrayElement(1)
          )
      );

    /*
    one could do like this:

    pgContext
      .eval(JavaScriptLanguage.ID, """
        (function(name, helper) {
          Handlebars.registerHelper(name, helper)
        })
        """)
      .execute(helperName, helper);

    but directly using java code as helper needs setting following EXPERIMENTAL flags
    while creating context:

      // ...
      .allowExperimentalOptions(true)
      .option(FOREIGN_OBJECT_PROTOTYPE_NAME, "true")
      // ...
     */
  }

  /**
   * @return whole console output from begin until now
   */
  public String getConsoleOutput() {
    return Optional
      .ofNullable(outputStream)
      .map(stream -> {
        String output = stream.toString(StandardCharsets.UTF_8);
        stream.reset();
        return output;
      })
      .orElse("");
  }

}
