package com.mohamnag.jandlebars;

import org.graalvm.polyglot.Value;

@FunctionalInterface
public interface Helper {

  /**
   * Called by Handlebars on template evaluation.
   *
   * @param context of current usage of helper
   * @param options passed to helper by Handlebars
   * @return processing result
   */
  String apply(Value context, Value options);

}
