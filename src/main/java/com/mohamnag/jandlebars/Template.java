package com.mohamnag.jandlebars;

import org.graalvm.polyglot.Value;

public class Template {

  private final ProxyGen proxyGen;
  private final Value templateFunction;

  Template(ProxyGen proxyGen, Value templateFunction) {
    this.proxyGen = proxyGen;
    this.templateFunction = templateFunction;
  }

  public String execute(Object templateContext) {
    return executeInternal(proxyGen.proxy(templateContext));
  }

  private String executeInternal(Object proxyObject) {
    Value executionResult = templateFunction.execute(proxyObject);

    if (!executionResult.isString()) {
      throw new RuntimeException("expected an string as result!");
    }

    return executionResult.asString();
  }

}
