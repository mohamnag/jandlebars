package com.mohamnag.jandlebars;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.proxy.Proxy;
import org.graalvm.polyglot.proxy.ProxyArray;
import org.graalvm.polyglot.proxy.ProxyObject;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Central place for generating proxy classes for all types of objects that are supported
 * as (part of) template context.
 */
class ProxyGen {

  /**
   * basic types properly handled by GraalJS
   */
  private static final Set<Class<?>> BASIC_TYPES = getBasicTypes();
  private final Context polyglotContext;

  public ProxyGen(Context pgContext) {
    polyglotContext = pgContext;
  }

  private static boolean isBasicType(Class<?> clazz) {
    return BASIC_TYPES.contains(clazz);
  }

  private static Set<Class<?>> getBasicTypes() {
    Set<Class<?>> ret = new HashSet<>();
    ret.add(Boolean.class);
    ret.add(Character.class);
    ret.add(Byte.class);
    ret.add(Short.class);
    ret.add(Integer.class);
    ret.add(Long.class);
    ret.add(Float.class);
    ret.add(Double.class);
    ret.add(Void.class);
    ret.add(String.class);
    return ret;
  }

  Object proxy(Object object) {
    if (Objects.isNull(object)) {
      return null;

    } else if (object instanceof Proxy) {
      return object;

    } else if (isBasicType(object.getClass())) {
      return object;

    } else if (object.getClass().isArray()) {
      return proxyArray((Object[]) object);

    } else if (object instanceof List) {
      return proxyList((List<Object>) object);

    } else if (object instanceof LocalDate) {
      return object.toString();
//      return ProxyDate.from((LocalDate) object);

    } else if (object instanceof Duration) {
      return object;
//      return ProxyDuration.from((Duration) object);

    } else if (object instanceof Instant) {
      return object.toString();
//      return ProxyInstant.from((Instant) object);

    } else if (object instanceof LocalTime) {
      return object.toString();
//      return ProxyTime.from((LocalTime) object);

    } else if (object instanceof ZoneId) {
      return object;
//      return ProxyTimeZone.from((ZoneId) object);

    } else if (object instanceof Map) {
      return proxyMap((Map<String, Object>) object);

    } else {
      return proxyPojo(object);
    }
  }

  private Proxy proxyPojo(Object templateContext) {
    return ProxyPojo.fromPojo(polyglotContext.asValue(templateContext), this);
  }

  private ProxyArray proxyArray(Object[] object) {
    var proxiedArray = Arrays
      .stream(object)
      .map(this::proxy)
      .toArray();

    return ProxyArray.fromArray(proxiedArray);
  }

  private ProxyArray proxyList(List<Object> object) {
    var proxiedList = object
      .stream()
      .map(this::proxy)
      .collect(Collectors.toList());

    return ProxyArray.fromList(proxiedList);
  }

  private Proxy proxyMap(Map<String, Object> object) {
    var proxiedContext = object
      .entrySet()
      .stream()
      .map(entry -> Map.entry(entry.getKey(), proxy(entry.getValue())))
      .collect(
        HashMap<String, Object>::new,
        (map, entry) -> map.put(entry.getKey(), entry.getValue()),
        HashMap::putAll
      );

    return ProxyObject.fromMap(proxiedContext);
  }
}
