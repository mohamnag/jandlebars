package com.mohamnag.jandlebars;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyObject;

import java.util.ArrayList;
import java.util.List;

public class ProxyPojo implements ProxyObject {

  private final Value delegate;
  private final ProxyGen proxyGen;

  ProxyPojo(Value delegate, ProxyGen proxyGen) {
    this.delegate = delegate;
    this.proxyGen = proxyGen;
  }

  public static ProxyObject fromPojo(Value javaBean, ProxyGen proxyGen) {
    if (javaBean.isHostObject() && javaBean.asHostObject() instanceof ProxyPojo) {
      return javaBean.asHostObject();
    } else {
      return new ProxyPojo(javaBean, proxyGen);
    }
  }

  @Override
  public Object getMember(String key) {
    Value getter = getGetter(key);

    Value value;
    if (getter != null && getter.canExecute()) {
      value = getter.execute();
    } else {
      value = delegate.getMember(key);
    }

    return value.isHostObject() ? proxyGen.proxy(value.asHostObject()) : value;
  }

  @Override
  public Object getMemberKeys() {
    List<String> members = new ArrayList<>();
    for (String key : delegate.getMemberKeys()) {
      if (getGetter(key) != null) {
        members.add(key);
      } else {
        members.add(key);
      }
    }
    return members;
  }

  @Override
  public boolean hasMember(String key) {
    return getGetter(key) != null || delegate.hasMember(key);
  }

  @Override
  public void putMember(String key, Value value) {
    Value setter = getSetter(key);
    if (setter != null && setter.canExecute()) {
      setter.execute(value);
    } else {
      delegate.putMember(key, value);
    }
  }

  private Value getGetter(String key) {
    Value getter = delegate.getMember("get" + firstLetterUpperCase(key));
    if (getter == null) {
      getter = delegate.getMember("is" + firstLetterUpperCase(key));
    }
    return getter;
  }

  private Value getSetter(String key) {
    return delegate.getMember("set" + firstLetterUpperCase(key));
  }

  private String firstLetterUpperCase(String name) {
    if (name.isEmpty()) {
      return name;
    }

    return Character.toUpperCase(name.charAt(0)) + name.substring(1);
  }
}
