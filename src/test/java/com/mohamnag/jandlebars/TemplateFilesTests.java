package com.mohamnag.jandlebars;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TemplateFilesTests {

  @Test
  public void loadTemplateFromFile() throws IOException {
    var jandlebars = new Jandlebars();

    var compiledTemplate = jandlebars.compileTemplate(Path.of("src/test/resources/test.hbs"));
    var result = compiledTemplate.execute(Map.of(
      "person", Map.of(
        "firstname", "Mo"
      )
    ));

    assertEquals("  person's first name is: Mo\n", result);
  }

  // TODO: 15.04.21 add test for templates that include other templates and use partials

}
