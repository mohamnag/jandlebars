package com.mohamnag.jandlebars;

import org.junit.Test;

import java.time.*;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ContextTests {

  @Test
  public void simpleMapContext() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "Handlebars <b>{{doesWhat}}</b>";
    var compiledTemplate = jandlebars.compileTemplate(template);

    // using a template
    // context: a Java Map
    Map<String, Object> templateContext = Map.of("doesWhat", "rocks!");
    var result = compiledTemplate.execute(templateContext);

    assertEquals("Handlebars <b>rocks!</b>", result);
  }

  @Test
  public void nestedMapContext() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "{{person.firstName}} {{person.lastName}}";
    var compiledTemplate = jandlebars.compileTemplate(template);

    // using a template
    // context: a nested Java Map
    var result = compiledTemplate.execute(Map.of(
      "person", Map.of(
        "firstName", "Yehuda",
        "lastName", "Katz"
      )
    ));

    assertEquals("Yehuda Katz", result);
  }

  @Test
  public void pojoContext() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "{{firstName}} {{lastName}}";
    var compiledTemplate = jandlebars.compileTemplate(template);

    // using a template
    // context: a nested Java Map
    var result = compiledTemplate.execute(
      new SimplePojo("Yehuda", "Katz")
    );

    assertEquals("got Yehuda Katz", result);
  }

  @Test
  public void pojosInMapContext() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "{{person.firstName}} {{person.lastName}}";
    var compiledTemplate = jandlebars.compileTemplate(template);

    // using a template
    // context: a nested Java Map
    var result = compiledTemplate.execute(Map.of(
      "person", new SimplePojo("Yehuda", "Katz")
    ));

    assertEquals("got Yehuda Katz", result);
  }

  @Test
  public void mapInPojoContext() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "{{this.tools.build}} uses {{tools.compile}}";
    var compiledTemplate = jandlebars.compileTemplate(template);

    // using a template
    // context: a nested Java Map
    var result = compiledTemplate.execute(new PojoWithMap(
      Map.of(
        "build", "Gradle",
        "compile", "javac"
      )
    ));

    assertEquals("Gradle uses javac", result);
  }

  @Test
  public void localDateContext() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "it was evening of {{this}}";
    var compiledTemplate = jandlebars.compileTemplate(template);

    // using a template
    // context: a nested Java Map
    var result = compiledTemplate.execute(LocalDate.parse("2020-11-09"));

    assertEquals("it was evening of 2020-11-09", result);
  }

  @Test
  public void instantContext() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "it was evening of {{this}}";
    var compiledTemplate = jandlebars.compileTemplate(template);

    // using a template
    // context: a nested Java Map
    var result = compiledTemplate.execute(Instant.parse("2020-11-09T18:05:10.241576Z"));

    assertEquals("it was evening of 2020-11-09T18:05:10.241576Z", result);
  }

  @Test
  public void localTimeContext() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "it was {{this}}";
    var compiledTemplate = jandlebars.compileTemplate(template);

    // using a template
    // context: a nested Java Map
    var result = compiledTemplate.execute(LocalTime.parse("19:12:18.445919"));

    assertEquals("it was 19:12:18.445919", result);
  }

  @Test
  public void zoneIdContext() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "we are in {{this}}";
    var compiledTemplate = jandlebars.compileTemplate(template);

    // using a template
    // context: a nested Java Map
    var result = compiledTemplate.execute(ZoneId.of("Europe/Vienna"));

    assertEquals("we are in Europe/Vienna", result);
  }

  @Test
  public void durationContext() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "it was {{this}} long";
    var compiledTemplate = jandlebars.compileTemplate(template);

    // using a template
    // context: a nested Java Map
    var result = compiledTemplate.execute(Duration.ofDays(3).plusHours(5));

    assertEquals("it was PT77H long", result);
  }

  public static class SimplePojo {
    private final String firstName;
    public String lastName;

    public SimplePojo(String firstName, String lastName) {
      this.firstName = firstName;
      this.lastName = lastName;
    }

    public String getFirstName() {
      return "got " + firstName;
    }
  }

  public static class PojoWithMap {
    private final Map<String, Object> tools;

    public PojoWithMap(Map<String, Object> tools) {
      this.tools = tools;
    }

    public Map<String, Object> getTools() {
      return tools;
    }
  }
}
