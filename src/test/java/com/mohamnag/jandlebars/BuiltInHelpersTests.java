package com.mohamnag.jandlebars;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BuiltInHelpersTests {
  @Test
  public void logHelper() throws Exception {
    var jandlebars = new Jandlebars(true);

    var template = jandlebars.compileTemplate("{{log 'this is a simple log output'}}");
    var result = template.execute(null);
    var logs = jandlebars.getConsoleOutput();

    assertEquals("", result);
    assertEquals("this is a simple log output\n", logs);
  }

  // TODO: 15.04.21 add tests for all built-in helpers

}
