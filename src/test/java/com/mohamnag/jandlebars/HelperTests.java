package com.mohamnag.jandlebars;

import org.junit.Test;

import java.nio.file.Path;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HelperTests {

  @Test
  public void inlineJsHelper() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "Handlebars <b>{{loud doesWhat}}</b>";
    var compiledTemplate = jandlebars.compileTemplate(template);

    jandlebars.registerJavaScriptHelper(
      "loud",
      """
        function (aString) {
            return aString.toUpperCase()
        }"""
    );

    // using a template
    // context: a Java Map
    Map<String, Object> templateContext = Map.of("doesWhat", "rocks!");
    var result = compiledTemplate.execute(templateContext);

    assertEquals("Handlebars <b>ROCKS!</b>", result);
  }

  @Test
  public void jsFileHelper() throws Exception {
    var jandlebars = new Jandlebars();

    // load a helper file
    jandlebars
      .registerJavaScriptHelper(Path.of("src/test/resources/helpers.js"));

    // compiling the template
    var compiledTemplate = jandlebars.compileTemplate("Handlebars <b>{{loud doesWhat}}</b>");

    var result = compiledTemplate.execute(Map.of("doesWhat", "rocks!"));
    assertEquals("Handlebars <b>ROCKS!</b>", result);

    compiledTemplate = jandlebars.compileTemplate("{{#goodbyes}}{{{link ../prefix}}}{{/goodbyes}}");
    result = compiledTemplate.execute(
      Map.of(
        "prefix", "/root",
        "goodbyes", new Object[]{
          Map.of(
            "url", "goodbye",
            "text", "Goodbye"
          )
        }
      )
    );

    assertEquals("<a href='/root/goodbye'>Goodbye</a>", result);
  }

  @Test
  public void javaHelper() throws Exception {
    var jandlebars = new Jandlebars();

    // compiling the template
    var template = "today's topic is {{up this}}!";
    var compiledTemplate = jandlebars.compileTemplate(template);

    jandlebars.registerJavaHelper(
      "up",
      (context, options) -> {
        assertTrue(context.getMemberKeys().contains("topic"));
        assertTrue(context.getMember("topic").isString());

        return context.getMember("topic").asString().toUpperCase();
      }
    );

    // using a template
    // context: a Java Map
    Map<String, Object> templateContext = Map.of("topic", "history");
    var result = compiledTemplate.execute(templateContext);

    assertEquals("today's topic is HISTORY!", result);
  }

}
