Handlebars.registerHelper('loud', function (aString) {
  return aString.toUpperCase()
})

Handlebars.registerHelper('link', function (prefix) {
  return "<a href='" + prefix + "/" + this.url + "'>" + this.text  + "</a>";
});
