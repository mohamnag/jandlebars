# Intro
Jandlebars is the simplest way of using Handlebars in a Java context taking advantage of modern
Java. It is in *no means* a port of original [Handlebars.js](https://handlebarsjs.com/)
in Java but rather uses the GraalJS to run the JavaScript engine in a JVM context and provides
very thin wrapper around it to make its usage easy.

The advantage of this approach is that the wheel will not be reinvented, and both Handlebars and
Java could be in their latest version.

For usage see [tests](src/test/java/com/mohamnag/jandlebars).

Why would you use this?
- you need to compile and use Handlebars templates in Java (obvious one)
- you want to reuse available JavaScript handlers
- you need Java interop
    - using Java objects as template context
    - using Java code for helpers

## Supported Context Data Types
you can use following data types as template context:
- all primitive types
- instances of `java.lang.String`
- any POJO
    - both public fields and bean style accessors (`getProperty`, `isProperty`) can be used just by using property name
- instances of `java.util.Map<String, Object>` whereas `Object` can be any of valid types
- instances of `java.time.LocalDate` (passed as string)
- instances of `java.time.Duration`
- instances of `java.time.Instant`
- instances of `java.time.LocalTime`
- instances of `java.time.ZoneId`
- array of valid types
- List of valid types

## Roadmap
Following additional features are/will be added incrementally:
- [ ] caching compiled templates
